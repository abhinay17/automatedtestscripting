# Automated_Test_Scripting

This is the repository for automated test scripts using Python's pytest library. Just to reiterate the steps in the project (as of April 9, 2018):

1. Download files from data sources and dump them in S3 buckets. 
2. Convert the files to JSON and split them into individual files, as necessary.
3. Using streamsets, send the JSON data to a MapR database. 
4. Query necessary information and initiate the process of report generation. Reports are the final end product desired by the client, as according to requirements until June 2018.

This repository will primarly have Python files that use the pytest library to test specific parts of the code built by developers. 

